#pragma once

#include "esp_err.h"
#include "esp_log.h"
#include "nvs.h"

template <typename T> T load_app_data(const char name[16]) {
  T data;
  nvs_handle_t my_handle;
  esp_err_t err;
  size_t data_size = sizeof(T);
  err = nvs_open(name, NVS_READWRITE, &my_handle);
  if (err != ESP_OK)
    return data;

  err = nvs_get_blob(my_handle, name, static_cast<void *>(&data), &data_size);
  if (err != ESP_OK && err != ESP_ERR_NVS_NOT_FOUND)
    return data;
  nvs_close(my_handle);
  return data;
}

template <typename T> esp_err_t write_app_data(const char name[16], T *data) {
  nvs_handle_t my_handle;
  esp_err_t err;
  size_t data_size = sizeof(T);
  err = nvs_open(name, NVS_READWRITE, &my_handle);
  if (err != ESP_OK)
    return err;

  err = nvs_set_blob(my_handle, name, static_cast<void *>(data),
                     data_size);
  if (err != ESP_OK && err != ESP_ERR_NVS_NOT_FOUND)
    return err;
  nvs_close(my_handle);
  return ESP_OK;
}


#include "ota.h"

#define UPGRADE_URL "whitemilan.com"
#define HASH_LEN 32

static const char *tag = "ota";

extern const char
    lets_encrypt_pem_start[] asm("_binary_lets_encrypt_r3_pem_start");
static SemVer new_version;

#define OTA_URL_SIZE 256

esp_err_t ota_http_event_handler(esp_http_client_event_t *evt) {
  switch (evt->event_id) {
  case HTTP_EVENT_ERROR:
    ESP_LOGD(tag, "HTTP_EVENT_ERROR");
    break;
  case HTTP_EVENT_ON_CONNECTED:
    ESP_LOGD(tag, "HTTP_EVENT_ON_CONNECTED");
    break;
  case HTTP_EVENT_HEADER_SENT:
    ESP_LOGD(tag, "HTTP_EVENT_HEADER_SENT");
    break;
  case HTTP_EVENT_ON_HEADER:
    ESP_LOGD(tag, "HTTP_EVENT_ON_HEADER, key=%s, value=%s", evt->header_key,
             evt->header_value);
    break;
  case HTTP_EVENT_ON_DATA:
    ESP_LOGD(tag, "HTTP_EVENT_ON_DATA, len=%d", evt->data_len);
    break;
  case HTTP_EVENT_ON_FINISH:
    ESP_LOGD(tag, "HTTP_EVENT_ON_FINISH");
    break;
  case HTTP_EVENT_DISCONNECTED:
    ESP_LOGD(tag, "HTTP_EVENT_DISCONNECTED");
    break;
  case HTTP_EVENT_REDIRECT:
    ESP_LOGD(tag, "HTTP_EVENT_REDIRECT");
    break;
  }
  return ESP_OK;
}

void simple_ota_example_task(void *pvParameter) {
  ESP_LOGI(tag, "Starting OTA example task");

  std::string url("https://whitemilan.com/firmware/temperature_sensor/");
  url.append(new_version.to_string().c_str());
  url.append(".bin");
  ESP_LOGI(tag, "URL: %s", url.c_str());

  esp_http_client_config_t config = {
      .url = url.c_str(),
      .cert_pem = lets_encrypt_pem_start,
      .event_handler = ota_http_event_handler,
      .skip_cert_common_name_check = true,
      .keep_alive_enable = true,
  };

  esp_https_ota_config_t ota_config = {
      .http_config = &config,
  };
  // ESP_LOGI(tag, "Attempting to download update from %s", config.host);
  esp_err_t ret = esp_https_ota(&ota_config);
  if (ret == ESP_OK) {
    ESP_LOGI(tag, "OTA Succeed, Rebooting...");
    esp_restart();
  } else {
    ESP_LOGE(tag, "Firmware upgrade failed");
  }
  while (1) {
    vTaskDelay(1000 / portTICK_PERIOD_MS);
  }
}

static void print_sha256(const uint8_t *image_hash, const char *label) {
  char hash_print[HASH_LEN * 2 + 1];
  hash_print[HASH_LEN * 2] = 0;
  for (int i = 0; i < HASH_LEN; ++i) {
    sprintf(&hash_print[i * 2], "%02x", image_hash[i]);
  }
  ESP_LOGI(tag, "%s %s", label, hash_print);
}

static void get_sha256_of_partitions(void) {
  uint8_t sha_256[HASH_LEN] = {0};
  esp_partition_t partition;

  // get sha256 digest for bootloader
  partition.address = ESP_BOOTLOADER_OFFSET;
  partition.size = ESP_PARTITION_TABLE_OFFSET;
  partition.type = ESP_PARTITION_TYPE_APP;
  esp_partition_get_sha256(&partition, sha_256);
  print_sha256(sha_256, "SHA-256 for bootloader: ");

  // get sha256 digest for running partition
  esp_partition_get_sha256(esp_ota_get_running_partition(), sha_256);
  print_sha256(sha_256, "SHA-256 for current firmware: ");
}

esp_err_t ota() {

  auto response = get_request();
  ESP_LOGI(tag, "Response: %s", response.c_str());

  std::size_t start = 0;
  std::size_t end = response.find('\n');
  while (end != std::string::npos) {
    auto line = response.substr(start, end - start);
    start = end + 1;
    end = response.find('\n', start);
    if (line.substr(0, 3) != "<a ")
      continue;
    auto start = line.find_first_of('>');
    auto end = line.find_last_of('<');
    auto version_string = line.substr(start + 1, end);
    auto parsed_version = SemVer::fromString(&version_string);
    if (new_version < parsed_version)
      new_version = parsed_version;
  }
  auto v = new_version.to_string();
  ESP_LOGI(tag, "Version: %s", v.c_str());
  if (FIRMWARE_VERSION < new_version) {
    static std::string message;
    message.resize(64);
    sprintf(message.data(), "Updating to version: %s", v.c_str());
    post(&message);
    get_sha256_of_partitions();
    xTaskCreate(&simple_ota_example_task, "ota_example_task", 8192, NULL, 5,
                NULL);
    return true;
  }
  return false;
}

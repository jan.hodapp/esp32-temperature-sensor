#pragma once

#include "esp_log.h"
#include <string>
using namespace std;
static const char *TAG = "semantic version";

struct SemVer {
  uint8_t major = 0;
  uint8_t minor = 0;
  uint8_t patch = 0;

  static SemVer fromString(string *s) {
    ESP_LOGI(TAG, "SemVer from String: %s", s->c_str());
    auto first_dot = s->find('.');
    if (first_dot == string::npos) {
      return SemVer(stoi(*s));
    }
    auto second_dot = s->find('.', first_dot + 1);
    if (second_dot == string::npos) {
      return SemVer(stoi(s->substr(0, first_dot)),
                    stoi(s->substr(first_dot + 1)));
    }
    return SemVer(stoi(s->substr(0, first_dot)),
                  stoi(s->substr(first_dot + 1, second_dot)),
                  stoi(s->substr(second_dot + 1)));
  }

  friend bool operator<(const SemVer &v1, const SemVer &v2) {
    if (v1.major > v2.major)
      return false;
    else if (v1.major < v2.major)
      return true;
    if (v1.minor > v2.minor)
      return false;
    else if (v1.minor < v2.minor)
      return true;
    if (v1.patch > v2.patch)
      return false;
    else if (v1.patch < v2.patch)
      return true;
    return false;
  };

  friend bool operator>(const SemVer &v1, const SemVer &v2) { return v2 < v1; };

  friend bool operator<=(const SemVer &v1, const SemVer &v2) {
    return not(v2 < v1);
  };
  friend bool operator>=(const SemVer &v1, const SemVer &v2) {
    return not(v1 < v2);
  };
  friend bool operator==(const SemVer &v1, const SemVer &v2) {
    return v1.patch == v2.patch && v1.minor == v2.minor && v1.major == v2.major;
  };

  string to_string() const {
    return string(::to_string(major) + '.' + ::to_string(minor) + '.' +
                  ::to_string(patch));
  }
};

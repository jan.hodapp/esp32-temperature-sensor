#pragma once

#include "driver/gpio.h"
#include "esp_event.h"
#include "esp_log.h"
#include "esp_random.h"
#include <algorithm>
#include <array>
#include <numeric>
#include <optional>

class MAX6675 {
public:
  MAX6675(gpio_num_t gpio_sclk, gpio_num_t gpio_cs, gpio_num_t gpio_miso);

  std::optional<float> readCelsius(void);
  uint8_t spiread();

private:
  gpio_num_t sclk, miso, cs;
  uint8_t value = 0;
  std::array<float, 30> ys = {};
  SemaphoreHandle_t mutexValue;
  QueueHandle_t queueMiso = NULL;
  static void setValueStatic(void *pvParameter);
  void setValue();
};

#pragma once

#include "configuration.h"
#include "esp_wifi.h"
#include "event.h"
#include "freertos/event_groups.h"
#include "http_request.h"
#include "lwip/apps/sntp.h"
#include <set>
#include <vector>

enum State {
  Unconnected,
  ConnectedToStation,
  IpFromStation,
  AccessPoint,
  Scanning
};

struct WifiConfiguration {
  unsigned char ssid[32] = "";
  unsigned char password[64] = "";
};

class Wifi {
public:
  Wifi(WifiConfiguration, esp_event_loop_handle_t);
  ~Wifi();

  esp_err_t init_connection_to_station(WifiConfiguration);
  esp_err_t init_access_point();
  State state = State::Unconnected;
  std::set<std::string> scanned_ssids;
  std::set<std::string>* scan();

private:
  int ap_sta = 0;
  void from_ap_to_sta(void);
  void from_sta_to_ap(void);
  esp_netif_t *ap = NULL;
  esp_netif_t *sta = NULL;
  esp_event_loop_handle_t app;
  WifiConfiguration configuration;
};

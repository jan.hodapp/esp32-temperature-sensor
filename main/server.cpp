#include "server.h"
#include "esp_event_base.h"
#include "ota.h"
#include <math.h>
#include <tuple>

/* Type of Escape algorithms to be used */
#define NGX_ESCAPE_URI (0)
#define NGX_ESCAPE_ARGS (1)
#define NGX_ESCAPE_URI_COMPONENT (2)
#define NGX_ESCAPE_HTML (3)
#define NGX_ESCAPE_REFRESH (4)
#define NGX_ESCAPE_MEMCACHED (5)
#define NGX_ESCAPE_MAIL_AUTH (6)

/* Type of Unescape algorithms to be used */
#define NGX_UNESCAPE_URI (1)
#define NGX_UNESCAPE_REDIRECT (2)

#define EXAMPLE_HTTP_QUERY_KEY_MAX_LEN (64)

static const char *tag = "server";

extern const char app_page_start[] asm("_binary_app_html_start");
extern const char wifi_page_start[] asm("_binary_wifi_html_start");
extern const char icon_start[] asm("_binary_favicon_svg_start");
extern const char style_start[] asm("_binary_style_css_start");

static void replace(std::string *s, const char *to_be_replaced,
                    std::string *replacement) {
  std::size_t pos = s->find(to_be_replaced);

  if (pos != std::string::npos) {
    // ESP_LOI(tag, "Found it");
    s->replace(pos, strlen(to_be_replaced), replacement->data());
  }
}

uintptr_t ngx_escape_uri(u_char *dst, u_char *src, size_t size,
                         unsigned int type) {
  unsigned int n;
  uint32_t *escape;
  static u_char hex[] = "0123456789ABCDEF";

  /*
   * Per RFC 3986 only the following chars are allowed in URIs unescaped:
   *
   * unreserved    = ALPHA / DIGIT / "-" / "." / "_" / "~"
   * gen-delims    = ":" / "/" / "?" / "#" / "[" / "]" / "@"
   * sub-delims    = "!" / "$" / "&" / "'" / "(" / ")"
   *               / "*" / "+" / "," / ";" / "="
   *
   * And "%" can appear as a part of escaping itself.  The following
   * characters are not allowed and need to be escaped: %00-%1F, %7F-%FF,
   * " ", """, "<", ">", "\", "^", "`", "{", "|", "}".
   */

  /* " ", "#", "%", "?", not allowed */

  static uint32_t uri[] = {
      0xffffffff, /* 1111 1111 1111 1111  1111 1111 1111 1111 */

      /* ?>=< ;:98 7654 3210  /.-, +*)( '&%$ #"!  */
      0xd000002d, /* 1101 0000 0000 0000  0000 0000 0010 1101 */

      /* _^]\ [ZYX WVUT SRQP  ONML KJIH GFED CBA@ */
      0x50000000, /* 0101 0000 0000 0000  0000 0000 0000 0000 */

      /*  ~}| {zyx wvut srqp  onml kjih gfed cba` */
      0xb8000001, /* 1011 1000 0000 0000  0000 0000 0000 0001 */

      0xffffffff, /* 1111 1111 1111 1111  1111 1111 1111 1111 */
      0xffffffff, /* 1111 1111 1111 1111  1111 1111 1111 1111 */
      0xffffffff, /* 1111 1111 1111 1111  1111 1111 1111 1111 */
      0xffffffff  /* 1111 1111 1111 1111  1111 1111 1111 1111 */
  };

  /* " ", "#", "%", "&", "+", ";", "?", not allowed */

  static uint32_t args[] = {
      0xffffffff, /* 1111 1111 1111 1111  1111 1111 1111 1111 */

      /* ?>=< ;:98 7654 3210  /.-, +*)( '&%$ #"!  */
      0xd800086d, /* 1101 1000 0000 0000  0000 1000 0110 1101 */

      /* _^]\ [ZYX WVUT SRQP  ONML KJIH GFED CBA@ */
      0x50000000, /* 0101 0000 0000 0000  0000 0000 0000 0000 */

      /*  ~}| {zyx wvut srqp  onml kjih gfed cba` */
      0xb8000001, /* 1011 1000 0000 0000  0000 0000 0000 0001 */

      0xffffffff, /* 1111 1111 1111 1111  1111 1111 1111 1111 */
      0xffffffff, /* 1111 1111 1111 1111  1111 1111 1111 1111 */
      0xffffffff, /* 1111 1111 1111 1111  1111 1111 1111 1111 */
      0xffffffff  /* 1111 1111 1111 1111  1111 1111 1111 1111 */
  };

  /* not ALPHA, DIGIT, "-", ".", "_", "~" */

  static uint32_t uri_component[] = {
      0xffffffff, /* 1111 1111 1111 1111  1111 1111 1111 1111 */

      /* ?>=< ;:98 7654 3210  /.-, +*)( '&%$ #"!  */
      0xfc009fff, /* 1111 1100 0000 0000  1001 1111 1111 1111 */

      /* _^]\ [ZYX WVUT SRQP  ONML KJIH GFED CBA@ */
      0x78000001, /* 0111 1000 0000 0000  0000 0000 0000 0001 */

      /*  ~}| {zyx wvut srqp  onml kjih gfed cba` */
      0xb8000001, /* 1011 1000 0000 0000  0000 0000 0000 0001 */

      0xffffffff, /* 1111 1111 1111 1111  1111 1111 1111 1111 */
      0xffffffff, /* 1111 1111 1111 1111  1111 1111 1111 1111 */
      0xffffffff, /* 1111 1111 1111 1111  1111 1111 1111 1111 */
      0xffffffff  /* 1111 1111 1111 1111  1111 1111 1111 1111 */
  };

  /* " ", "#", """, "%", "'", not allowed */

  static uint32_t html[] = {
      0xffffffff, /* 1111 1111 1111 1111  1111 1111 1111 1111 */

      /* ?>=< ;:98 7654 3210  /.-, +*)( '&%$ #"!  */
      0x500000ad, /* 0101 0000 0000 0000  0000 0000 1010 1101 */

      /* _^]\ [ZYX WVUT SRQP  ONML KJIH GFED CBA@ */
      0x50000000, /* 0101 0000 0000 0000  0000 0000 0000 0000 */

      /*  ~}| {zyx wvut srqp  onml kjih gfed cba` */
      0xb8000001, /* 1011 1000 0000 0000  0000 0000 0000 0001 */

      0xffffffff, /* 1111 1111 1111 1111  1111 1111 1111 1111 */
      0xffffffff, /* 1111 1111 1111 1111  1111 1111 1111 1111 */
      0xffffffff, /* 1111 1111 1111 1111  1111 1111 1111 1111 */
      0xffffffff  /* 1111 1111 1111 1111  1111 1111 1111 1111 */
  };

  /* " ", """, "'", not allowed */

  static uint32_t refresh[] = {
      0xffffffff, /* 1111 1111 1111 1111  1111 1111 1111 1111 */

      /* ?>=< ;:98 7654 3210  /.-, +*)( '&%$ #"!  */
      0x50000085, /* 0101 0000 0000 0000  0000 0000 1000 0101 */

      /* _^]\ [ZYX WVUT SRQP  ONML KJIH GFED CBA@ */
      0x50000000, /* 0101 0000 0000 0000  0000 0000 0000 0000 */

      /*  ~}| {zyx wvut srqp  onml kjih gfed cba` */
      0xd8000001, /* 1011 1000 0000 0000  0000 0000 0000 0001 */

      0xffffffff, /* 1111 1111 1111 1111  1111 1111 1111 1111 */
      0xffffffff, /* 1111 1111 1111 1111  1111 1111 1111 1111 */
      0xffffffff, /* 1111 1111 1111 1111  1111 1111 1111 1111 */
      0xffffffff  /* 1111 1111 1111 1111  1111 1111 1111 1111 */
  };

  /* " ", "%", %00-%1F */

  static uint32_t memcached[] = {
      0xffffffff, /* 1111 1111 1111 1111  1111 1111 1111 1111 */

      /* ?>=< ;:98 7654 3210  /.-, +*)( '&%$ #"!  */
      0x00000021, /* 0000 0000 0000 0000  0000 0000 0010 0001 */

      /* _^]\ [ZYX WVUT SRQP  ONML KJIH GFED CBA@ */
      0x00000000, /* 0000 0000 0000 0000  0000 0000 0000 0000 */

      /*  ~}| {zyx wvut srqp  onml kjih gfed cba` */
      0x00000000, /* 0000 0000 0000 0000  0000 0000 0000 0000 */

      0x00000000, /* 0000 0000 0000 0000  0000 0000 0000 0000 */
      0x00000000, /* 0000 0000 0000 0000  0000 0000 0000 0000 */
      0x00000000, /* 0000 0000 0000 0000  0000 0000 0000 0000 */
      0x00000000, /* 0000 0000 0000 0000  0000 0000 0000 0000 */
  };

  /* mail_auth is the same as memcached */

  static uint32_t *map[] = {uri,     args,      uri_component, html,
                            refresh, memcached, memcached};

  escape = map[type];

  if (dst == NULL) {

    /* find the number of the characters to be escaped */

    n = 0;

    while (size) {
      if (escape[*src >> 5] & (1U << (*src & 0x1f))) {
        n++;
      }
      src++;
      size--;
    }

    return (uintptr_t)n;
  }

  while (size) {
    if (escape[*src >> 5] & (1U << (*src & 0x1f))) {
      *dst++ = '%';
      *dst++ = hex[*src >> 4];
      *dst++ = hex[*src & 0xf];
      src++;

    } else {
      *dst++ = *src++;
    }
    size--;
  }

  return (uintptr_t)dst;
}

void ngx_unescape_uri(u_char **dst, u_char **src, size_t size,
                      unsigned int type) {
  u_char *d, *s, ch, c, decoded;
  enum { sw_usual = 0, sw_quoted, sw_quoted_second } state;

  d = *dst;
  s = *src;

  state = sw_usual;
  decoded = 0;

  while (size--) {

    ch = *s++;

    switch (state) {
    case sw_usual:
      if (ch == '?' && (type & (NGX_UNESCAPE_URI | NGX_UNESCAPE_REDIRECT))) {
        *d++ = ch;
        goto done;
      }

      if (ch == '%') {
        state = sw_quoted;
        break;
      }

      if (ch == '+') {
        *d++ = ' ';
        break;
      }

      *d++ = ch;
      break;

    case sw_quoted:

      if (ch >= '0' && ch <= '9') {
        decoded = (u_char)(ch - '0');
        state = sw_quoted_second;
        break;
      }

      c = (u_char)(ch | 0x20);
      if (c >= 'a' && c <= 'f') {
        decoded = (u_char)(c - 'a' + 10);
        state = sw_quoted_second;
        break;
      }

      /* the invalid quoted character */

      state = sw_usual;

      *d++ = ch;

      break;

    case sw_quoted_second:

      state = sw_usual;

      if (ch >= '0' && ch <= '9') {
        ch = (u_char)((decoded << 4) + (ch - '0'));

        if (type & NGX_UNESCAPE_REDIRECT) {
          if (ch > '%' && ch < 0x7f) {
            *d++ = ch;
            break;
          }

          *d++ = '%';
          *d++ = *(s - 2);
          *d++ = *(s - 1);

          break;
        }

        *d++ = ch;

        break;
      }

      c = (u_char)(ch | 0x20);
      if (c >= 'a' && c <= 'f') {
        ch = (u_char)((decoded << 4) + (c - 'a') + 10);

        if (type & NGX_UNESCAPE_URI) {
          if (ch == '?') {
            *d++ = ch;
            goto done;
          }

          *d++ = ch;
          break;
        }

        if (type & NGX_UNESCAPE_REDIRECT) {
          if (ch == '?') {
            *d++ = ch;
            goto done;
          }

          if (ch > '%' && ch < 0x7f) {
            *d++ = ch;
            break;
          }

          *d++ = '%';
          *d++ = *(s - 2);
          *d++ = *(s - 1);
          break;
        }

        *d++ = ch;

        break;
      }

      /* the invalid quoted character */

      break;
    }
  }

done:

  *dst = d;
  *src = s;
}

uint32_t example_uri_encode(char *dest, const char *src, size_t len) {
  if (!src || !dest) {
    return 0;
  }

  uintptr_t ret = ngx_escape_uri((unsigned char *)dest, (unsigned char *)src,
                                 len, NGX_ESCAPE_URI_COMPONENT);
  return (uint32_t)(ret - (uintptr_t)dest);
}

void example_uri_decode(char *dest, const char *src, size_t len) {
  if (!src || !dest) {
    return;
  }

  unsigned char *src_ptr = (unsigned char *)src;
  unsigned char *dst_ptr = (unsigned char *)dest;
  ngx_unescape_uri(&dst_ptr, &src_ptr, len, NGX_UNESCAPE_URI);
}

static std::optional<std::string> get_url_parameter(char *buffer,
                                                    std::string key) {
  char param[EXAMPLE_HTTP_QUERY_KEY_MAX_LEN],
      dec_param[EXAMPLE_HTTP_QUERY_KEY_MAX_LEN] = {0};
  if (httpd_query_key_value(buffer, key.data(), param, sizeof(param)) !=
      ESP_OK) {
    return std::nullopt;
  }
  example_uri_decode(dec_param, param,
                     strnlen(param, EXAMPLE_HTTP_QUERY_KEY_MAX_LEN));
  std::string result(dec_param);
  return result;
}

static void check_query(httpd_req_t *req,
                        std::atomic<esp_event_loop_handle_t> *app) {
  char *buf;
  size_t buf_len;
  /* Read URL query string length and allocate memory for length + 1,
   * extra byte for null termination */
  buf_len = httpd_req_get_url_query_len(req) + 1;
  if (buf_len > 1) {
    buf = static_cast<char *>(malloc(buf_len));
    if (httpd_req_get_url_query_str(req, buf, buf_len) == ESP_OK) {
      ESP_LOGI(TAG, "Found URL query => %s", buf);
      /* Get value of expected key from query string */
      auto ssid = get_url_parameter(buf, "ssid");
      auto password = get_url_parameter(buf, "password");
      if (ssid.has_value()) {
        WifiConfiguration configuration;
        if (password.has_value()) {
          memcpy(configuration.password, password->c_str(),
                 password->length() + 1);
        }
        memcpy(configuration.ssid, ssid->c_str(), ssid->length());
        ESP_LOGI(TAG, "Pointer to app: %p", app);
        esp_event_post_to(app->load(), APP, App_Event::Wifi_Configuration,
                          &configuration, sizeof(WifiConfiguration), 0);
      }
      auto temperature_query_string = get_url_parameter(buf, "temperature");

      if (temperature_query_string.has_value()) {
        float temperature = std::stof(temperature_query_string.value_or("0"));
        esp_event_post_to(app->load(), APP,
                          App_Event::Temperature_Configuration, &temperature,
                          sizeof(temperature), 0);
      }
      free(buf);
    }
  }
}

static esp_err_t app_handler(httpd_req_t *req) {
  auto handles =
      static_cast<std::tuple<std::atomic<esp_event_loop_handle_t> *,
                             std::atomic<float> *, std::atomic<float> *> *>(
          req->user_ctx);
  auto app = std::get<0>(*handles);
  auto temperature = std::get<1>(*handles);
  auto alarm_temperature = std::get<2>(*handles);

  ESP_LOGI(tag, "Pointer: %p", temperature);
  ESP_LOGI(tag, "Pointer: %4.1f", temperature->load());

  std::string response = std::string(app_page_start);
  std::string replacement;
  sprintf(replacement.data(), "%6.1lf", temperature->load());
  replace(&response, "<!--current_temperature-->", &replacement);
  sprintf(replacement.data(), "%6.1lf", alarm_temperature->load());
  replace(&response, "<!--alarm_temperature-->", &replacement);

  std::string version = FIRMWARE_VERSION.to_string();
  replace(&response, "<!--firmware_version-->", &version);
  check_query(req, app);
  httpd_resp_send(req, response.c_str(), HTTPD_RESP_USE_STRLEN);
  return ESP_OK;
}

static esp_err_t wifi_handler(httpd_req_t *req) {
  ESP_LOGI(tag, "Wifi handler");
  auto user_data = static_cast<
      std::tuple<std::atomic<esp_event_loop_handle_t> *, std::string *> *>(
      req->user_ctx);
  auto app = std::get<std::atomic<esp_event_loop_handle_t> *>(*user_data);
  auto ssids = std::get<std::string *>(*user_data);

  auto response = std::string(wifi_page_start);
  std::string toReplace = "<!--ssids-->";
  replace(&response, "<!--ssids-->", ssids);

  std::string version = FIRMWARE_VERSION.to_string();
  replace(&response, "<!--firmware_version-->", &version);

  check_query(req, app);
  httpd_resp_send(req, response.c_str(), HTTPD_RESP_USE_STRLEN);
  return ESP_OK;
}

static esp_err_t css_handler(httpd_req_t *req) {
  ESP_LOGI(tag, "css handler");
  httpd_resp_set_type(req, "text/css");
  httpd_resp_send(req, style_start, HTTPD_RESP_USE_STRLEN);
  return ESP_OK;
}

static esp_err_t icon_handler(httpd_req_t *req) {
  ESP_LOGI(tag, "Icon handler");
  httpd_resp_set_type(req, "image/svg+xml");
  httpd_resp_send(req, icon_start, HTTPD_RESP_USE_STRLEN);
  return ESP_OK;
}

esp_err_t http_404_error_handler(httpd_req_t *req, httpd_err_code_t err) {
  if (strcmp("/", req->uri) == 0) {
    httpd_resp_send_err(req, HTTPD_404_NOT_FOUND, "/ URI is not available");
    /* Return ESP_OK to keep underlying socket open */
    return ESP_OK;
  }
  httpd_resp_send_err(req, HTTPD_404_NOT_FOUND, "Some 404 error message");
  return ESP_FAIL;
}

httpd_handle_t webserver_start(std::atomic<esp_event_loop_handle_t> *app,
                               std::set<std::string> *ssids,
                               std::atomic<float> *temperature,
                               std::atomic<float> *alarm_temperature) {
  httpd_handle_t server = NULL;
  httpd_config_t config = HTTPD_DEFAULT_CONFIG();
  config.lru_purge_enable = true;

  // Start the httpd server
  ESP_LOGI(tag, "Starting server on port: '%d'", config.server_port);
  if (httpd_start(&server, &config) == ESP_OK) {
    // Set URI handlers
    ESP_LOGI(tag, "Registering URI handlers: Pointer %6.4lf",
             temperature->load());
    ESP_LOGI(tag, "Registering URI handlers: Pointer %p", temperature);

    static auto root_user_data =
        std::make_tuple(app, temperature, alarm_temperature);
    static const httpd_uri_t root = {.uri = "/",
                                     .method = HTTP_GET,
                                     .handler = app_handler,
                                     .user_ctx = &root_user_data};
    httpd_register_uri_handler(server, &root);

    static std::string ssids_formatted = "";
    ESP_LOGI(tag, "Hellooooo: Pointer: %p", &ssids_formatted);
    for (std::string ssid : *ssids) {
      ESP_LOGI(tag, "SSID: %s", ssid.c_str());
      ssids_formatted.append(
          "<div class=\"w3-bar-item w3-button\" onclick=\"fillSsid('" + ssid +
          "')\">" + ssid + "</div>\n");
    };

    static auto wifi_user_data = std::make_tuple(app, &ssids_formatted);
    static const httpd_uri_t wifi_configuration = {.uri = "/wifi",
                                                   .method = HTTP_GET,
                                                   .handler = wifi_handler,
                                                   .user_ctx = &wifi_user_data};
    httpd_register_uri_handler(server, &wifi_configuration);
    static const httpd_uri_t css = {.uri = "/style.css",
                                    .method = HTTP_GET,
                                    .handler = css_handler,
                                    .user_ctx = NULL};
    httpd_register_uri_handler(server, &css);
    static const httpd_uri_t icon = {.uri = "/favicon.svg",
                                     .method = HTTP_GET,
                                     .handler = icon_handler,
                                     .user_ctx = NULL};
    httpd_register_uri_handler(server, &icon);
    return server;
  }

  ESP_LOGI(tag, "Error starting server!");
  return NULL;
}

void webserver_stop(httpd_handle_t server) { httpd_stop(server); }

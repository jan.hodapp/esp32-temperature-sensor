#include "event.h"

esp_event_loop_handle_t initialize_app_event_loop() {
  static esp_event_loop_handle_t app = NULL;
  esp_event_loop_args_t loop_args = {.queue_size =
                                         CONFIG_ESP_SYSTEM_EVENT_QUEUE_SIZE,
                                     .task_name = "app",
                                     .task_priority = ESP_TASKD_EVENT_PRIO,
                                     .task_stack_size = ESP_TASKD_EVENT_STACK,
                                     .task_core_id = 1};

  ESP_ERROR_CHECK(esp_event_loop_create(&loop_args, &app));
  return app;
}

#pragma once

#include "esp_err.h"
#include "esp_event.h"
#include "esp_http_client.h"
#include "esp_https_ota.h"
#include "esp_log.h"
#include "esp_ota_ops.h"
#include "esp_system.h"
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "http_request.h"
#include "semantic_version.h"
#include "string.h"
#include <string>
#include <sys/socket.h>

static const SemVer FIRMWARE_VERSION(1, 1, 2); // Initialize NVS

esp_err_t ota();

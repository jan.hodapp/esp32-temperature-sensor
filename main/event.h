#pragma once

#include "esp_event.h"
#include "esp_task.h"

esp_event_loop_handle_t initialize_app_event_loop();
ESP_EVENT_DEFINE_BASE(APP);
enum App_Event {
  Wifi_Configuration,
  Wifi_Connected,
  Temperature_Configuration,
  Button,
  Wifi_Disconnected
};

#pragma once

#include "event.h"
#include "wifi.h"
#include <atomic>
#include <esp_http_server.h>
#include <esp_log.h>
#include <optional>
#include <string>
#include <tuple>

httpd_handle_t webserver_start(std::atomic<esp_event_loop_handle_t>*, std::set<std::string>*, std::atomic<float>*, std::atomic<float>*);
void webserver_stop(httpd_handle_t);

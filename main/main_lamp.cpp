#include "esp_eth.h"
#include "esp_netif.h"
#include "nvs_flash.h"
#include "protocol_examples_common.h"
#include <esp_event.h>
#include <esp_log.h>
#include <esp_system.h>
#include <esp_wifi.h>
#include <nvs_flash.h>
#include <sys/param.h>

#include "mdns.h"
#include <esp_http_server.h>

// PWM Stuff
#include "../build/webui.h"
#include "driver/ledc.h"
#include "esp_err.h"
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include <stdio.h>

#define LEDC_HS_TIMER LEDC_TIMER_0
#define LEDC_HS_MODE LEDC_HIGH_SPEED_MODE
#define LEDC_HS_CH0_GPIO (4)
#define LEDC_HS_CH0_CHANNEL LEDC_CHANNEL_0
#define LEDC_HS_CH1_GPIO (27)
#define LEDC_HS_CH1_CHANNEL LEDC_CHANNEL_1
#define LEDC_LS_TIMER LEDC_TIMER_1
#define LEDC_LS_MODE LEDC_LOW_SPEED_MODE
#define LEDC_LS_CH2_GPIO (21)
#define LEDC_LS_CH2_CHANNEL LEDC_CHANNEL_2
#define LEDC_LS_CH3_GPIO (33)
#define LEDC_LS_CH3_CHANNEL LEDC_CHANNEL_3

#define LEDC_CH_NUM (4)
#define LEDC_TEST_DUTY (4000)
#define LEDC_TEST_FADE_TIME (3000)

#define LEDC_MAX_DUTY (8191)

const int LED_GPIOS[4] = {21, 33, 27, 4};
const ledc_channel_t LED_CHANNELS[4] = {LEDC_CHANNEL_0, LEDC_CHANNEL_1,
                                        LEDC_CHANNEL_2, LEDC_CHANNEL_3};
size_t LEVELS_SIZE = 4 * sizeof(uint16_t);
/* A simple example that demonstrates using websocket echo server
 */
static const char *TAG = "Tree of Light";

uint64_t TIME_LAST_MSG = 0;
SemaphoreHandle_t SEMA_TIME_LAST_MSG = NULL;
uint16_t *CURRENT_LEVELS;
SemaphoreHandle_t SEMA_CURRENT_LEVELS = NULL;

/*
 * Structure holding server handle
 * and internal socket fd in order
 * to use out of request send
 */
struct async_resp_arg {
  httpd_handle_t hd;
  int fd;
};

uint16_t *nvs_read_levels(char *name) {
  uint16_t *levels = malloc(LEVELS_SIZE);
  nvs_handle_t nvs_handle;
  ESP_LOGI(TAG, "Levels size %zu", LEVELS_SIZE);
  ESP_LOGI(TAG, "Length %zu", 4 * sizeof(uint16_t));
  ESP_ERROR_CHECK(nvs_open("light_levels", NVS_READWRITE, &nvs_handle));
  esp_err_t ret = nvs_get_blob(nvs_handle, name, levels, &LEVELS_SIZE);
  if (ret != ESP_OK) {
    ESP_LOGW(TAG, "Couldn't read levels from nvs. Error %x", ret);
    levels = calloc(4, sizeof(uint16_t));
  };

  ESP_LOGI(TAG, "Length %zu", LEVELS_SIZE);
  nvs_close(nvs_handle);
  ESP_LOGI(TAG, "Read levels %d %d %d %d", *levels, *(levels + 1),
           *(levels + 2), *(levels + 3));
  return levels;
}

void nvs_write_levels(char *name, uint16_t *values) {
  nvs_handle_t nvs_handle;
  ESP_LOGI(TAG, "Writing levels %d %d %d %d", *values, *(values + 1),
           *(values + 2), *(values + 3));
  ESP_ERROR_CHECK(nvs_open("light_levels", NVS_READWRITE, &nvs_handle));
  ESP_ERROR_CHECK(nvs_set_blob(nvs_handle, name, values, LEVELS_SIZE));
  ESP_ERROR_CHECK(nvs_commit(nvs_handle));
  nvs_close(nvs_handle);
}

/*
 * async send function, which we put into the httpd work queue
 */
static void ws_async_send(void *arg) {
  static const char *data = "Async data";
  struct async_resp_arg *resp_arg = arg;
  httpd_handle_t hd = resp_arg->hd;
  int fd = resp_arg->fd;
  httpd_ws_frame_t ws_pkt;
  memset(&ws_pkt, 0, sizeof(httpd_ws_frame_t));
  ws_pkt.payload = (uint8_t *)data;
  ws_pkt.len = strlen(data);
  ws_pkt.type = HTTPD_WS_TYPE_TEXT;

  httpd_ws_send_frame_async(hd, fd, &ws_pkt);
  free(resp_arg);
}

static esp_err_t trigger_async_send(httpd_handle_t handle, httpd_req_t *req) {
  struct async_resp_arg *resp_arg = malloc(sizeof(struct async_resp_arg));
  resp_arg->hd = req->handle;
  resp_arg->fd = httpd_req_to_sockfd(req);
  return httpd_queue_work(handle, ws_async_send, resp_arg);
}

static uint32_t lightLevelToDuty(float level) {
  if (level < 0) {
    level = 0;
  } else if (1 < level) {
    level = 1;
  }
  ESP_LOGI(TAG, "lightLevelToDuty: %d", (uint32_t)(8191 * level * level));
  return ((uint32_t)(8191 * level * level));
}

static void setLightOutput(int light, uint32_t duty) {
  ledc_set_duty_and_update(LEDC_HS_MODE, light, duty, 0);
}

static void setLight(uint8_t light, uint16_t lightness, uint16_t temperature) {
  uint32_t duty_warm =
      lightLevelToDuty(2 * (float)lightness * (65535 - (float)temperature) /
                       ((float)65535 * 65535));
  uint32_t duty_cold = lightLevelToDuty(
      2 * (float)lightness * (float)temperature / ((float)65535 * 65535));
  setLightOutput(light * 2, duty_warm);
  setLightOutput(light * 2 + 1, duty_cold);

  struct timeval tv_now;
  gettimeofday(&tv_now, NULL);
  int64_t time_us = (int64_t)tv_now.tv_sec * 1000000L + (int64_t)tv_now.tv_usec;

  xSemaphoreTake(SEMA_CURRENT_LEVELS, (TickType_t)10);
  CURRENT_LEVELS[light * 2] = lightness;
  CURRENT_LEVELS[light * 2 + 1] = temperature;
  xSemaphoreGive(SEMA_CURRENT_LEVELS);

  xSemaphoreTake(SEMA_TIME_LAST_MSG, (TickType_t)10);
  if (TIME_LAST_MSG + 10000 < time_us) {
    xSemaphoreTake(SEMA_CURRENT_LEVELS, (TickType_t)10);
    nvs_write_levels("last", CURRENT_LEVELS);
    xSemaphoreGive(SEMA_CURRENT_LEVELS);
    TIME_LAST_MSG = time_us;
  }
  xSemaphoreGive(SEMA_TIME_LAST_MSG);
}

static esp_err_t ws_msg_handler(httpd_req_t *req) {
  if (req->method == HTTP_GET) {
    ESP_LOGI(TAG, "Handshake done, the new connection was opened");
    return ESP_OK;
  }
  ESP_LOGI(TAG, "length %zu", req->content_len);
  httpd_ws_frame_t ws_pkt;
  uint8_t buf[8] = {0};
  memset(&ws_pkt, 0, sizeof(httpd_ws_frame_t));
  ws_pkt.payload = buf;
  /*ws_pkt.type = HTTPD_WS_TYPE_TEXT;*/
  ws_pkt.type = HTTPD_WS_TYPE_BINARY;
  esp_err_t ret = httpd_ws_recv_frame(req, &ws_pkt, 8);
  if (ret != ESP_OK) {
    ESP_LOGE(TAG, "httpd_ws_recv_frame failed to get frame len with %d", ret);
    return ret;
  }

  ret = httpd_ws_send_frame(req, &ws_pkt);
  if (ret != ESP_OK) {
    ESP_LOGE(TAG, "httpd_ws_send_frame failed with %d", ret);
  }

  uint8_t command = ws_pkt.payload[0];
  ESP_LOGI(TAG, "Command: %d", command);
  switch (command) {
  case 0: {
    uint8_t id = ws_pkt.payload[1];
    uint16_t lightness;
    memcpy(&lightness, buf + 2, sizeof(lightness));
    uint16_t temperature;
    memcpy(&temperature, buf + 4, sizeof(temperature));
    ESP_LOGI(TAG, "Message: %d %d %d %d", command, id, lightness, temperature);
    setLight(id, lightness, temperature);
    break;
  }
  case 1: {
    // Save scene
    uint8_t savePosition = ws_pkt.payload[1];
    if (savePosition < 4) {
      char saveName[8];
      snprintf(saveName, 8, "Scene %d", savePosition);
      ESP_LOGI(TAG, "Save with name %s", saveName);
      xSemaphoreTake(SEMA_CURRENT_LEVELS, (TickType_t)10);
      nvs_write_levels(saveName, CURRENT_LEVELS);
      xSemaphoreGive(SEMA_CURRENT_LEVELS);
    } else {
      ESP_LOGE(TAG, "Save position out of range.");
    }
    break;
  }
  case 2: {
    // Load scene
    uint8_t loadPosition = ws_pkt.payload[1];
    if (loadPosition < 4) {
      char loadName[8];
      snprintf(loadName, 8, "Scene %d", loadPosition);
      ESP_LOGI(TAG, "Load with name %s", loadName);
      uint16_t *levels = nvs_read_levels(loadName);

      setLight(0, *levels, *(levels + 1));
      setLight(1, *(levels + 2), *(levels + 3));
      free(levels);

    } else {
      ESP_LOGE(TAG, "Load position out of range.");
    }
  }
  }
  return ret;
}

static const httpd_uri_t ws = {.uri = "/ws",
                               .method = HTTP_GET,
                               .handler = ws_msg_handler,
                               .user_ctx = NULL,
                               .is_websocket = true};

static esp_err_t http_get_handler(httpd_req_t *req) {
  char *buf;
  size_t buf_len;

  /* Get header value string length and allocate memory for length + 1,
   * extra byte for null termination */
  buf_len = httpd_req_get_hdr_value_len(req, "Host") + 1;
  if (buf_len > 1) {
    buf = malloc(buf_len);
    /* Copy null terminated value string into buffer */
    if (httpd_req_get_hdr_value_str(req, "Host", buf, buf_len) == ESP_OK) {
      ESP_LOGI(TAG, "Found header => Host: %s", buf);
    }
    free(buf);
  }

  buf_len = httpd_req_get_hdr_value_len(req, "Test-Header-2") + 1;
  if (buf_len > 1) {
    buf = malloc(buf_len);
    if (httpd_req_get_hdr_value_str(req, "Test-Header-2", buf, buf_len) ==
        ESP_OK) {
      ESP_LOGI(TAG, "Found header => Test-Header-2: %s", buf);
    }
    free(buf);
  }

  buf_len = httpd_req_get_hdr_value_len(req, "Test-Header-1") + 1;
  if (buf_len > 1) {
    buf = malloc(buf_len);
    if (httpd_req_get_hdr_value_str(req, "Test-Header-1", buf, buf_len) ==
        ESP_OK) {
      ESP_LOGI(TAG, "Found header => Test-Header-1: %s", buf);
    }
    free(buf);
  }

  /* Read URL query string length and allocate memory for length + 1,
   * extra byte for null termination */
  buf_len = httpd_req_get_url_query_len(req) + 1;
  if (buf_len > 1) {
    buf = malloc(buf_len);
    if (httpd_req_get_url_query_str(req, buf, buf_len) == ESP_OK) {
      ESP_LOGI(TAG, "Found URL query => %s", buf);
      char param[32];
      /* Get value of expected key from query string */
      if (httpd_query_key_value(buf, "query1", param, sizeof(param)) ==
          ESP_OK) {
        ESP_LOGI(TAG, "Found URL query parameter => query1=%s", param);
      }
      if (httpd_query_key_value(buf, "query3", param, sizeof(param)) ==
          ESP_OK) {
        ESP_LOGI(TAG, "Found URL query parameter => query3=%s", param);
      }
      if (httpd_query_key_value(buf, "query2", param, sizeof(param)) ==
          ESP_OK) {
        ESP_LOGI(TAG, "Found URL query parameter => query2=%s", param);
      }
    }
    free(buf);
  }

  /* Set some custom headers */
  /*httpd_resp_set_hdr(req, "Custom-Header-1", "Custom-Value-1");*/
  /*httpd_resp_set_hdr(req, "Custom-Header-2", "Custom-Value-2");*/

  /* Send response with custom headers and body set as the
   * string passed in user context*/
  const char *resp_str = (const char *)req->user_ctx;
  httpd_resp_send(req, resp_str, HTTPD_RESP_USE_STRLEN);

  /* After sending the HTTP response the old HTTP request
   * headers are lost. Check if HTTP request headers can be read now. */
  if (httpd_req_get_hdr_value_len(req, "Host") == 0) {
    ESP_LOGI(TAG, "Request headers lost");
  }
  return ESP_OK;
}

static const httpd_uri_t web_ui = {.uri = "/",
                                   .method = HTTP_GET,
                                   .handler = http_get_handler,
                                   /* Let's pass response string in user
                                    * context to demonstrate it's usage */
                                   .user_ctx = webui};

static httpd_handle_t start_webserver(void) {
  httpd_handle_t server = NULL;
  httpd_config_t config = HTTPD_DEFAULT_CONFIG();

  // Start the httpd server
  ESP_LOGI(TAG, "Starting server on port : '%d' ", config.server_port);
  if (httpd_start(&server, &config) == ESP_OK) {
    // Registering the ws handler
    ESP_LOGI(TAG, "Registering URI handlers");
    httpd_register_uri_handler(server, &web_ui);
    httpd_register_uri_handler(server, &ws);
    return server;
  }

  ESP_LOGI(TAG, "Error starting server!");
  return NULL;
}

static void stop_webserver(httpd_handle_t server) {
  // Stop the httpd server
  httpd_stop(server);
}

static void disconnect_handler(void *arg, esp_event_base_t event_base,
                               int32_t event_id, void *event_data) {
  httpd_handle_t *server = (httpd_handle_t *)arg;
  if (*server) {
    ESP_LOGI(TAG, "Stopping webserver");
    stop_webserver(*server);
    *server = NULL;
  }
}

static void connect_handler(void *arg, esp_event_base_t event_base,
                            int32_t event_id, void *event_data) {
  httpd_handle_t *server = (httpd_handle_t *)arg;
  if (*server == NULL) {
    ESP_LOGI(TAG, "Starting webserver");
    *server = start_webserver();
  }
}

// static void initialise_mdns(void) {
// char *hostname = "branch-lamp";
//// initialize mDNS
// ESP_ERROR_CHECK(mdns_init());
//// set mDNS hostname (required if you want to advertise services)
// ESP_ERROR_CHECK(
//[>mdns_service_add("WebUI", "_http", "_tcp", 80, serviceTxtData, 3));<]
// mdns_service_add("WebUI", "_http", "_tcp", 80, NULL, 0));
// ESP_ERROR_CHECK(mdns_hostname_set(hostname));
// ESP_LOGI(TAG, "mdns hostname set to: [%s]", hostname);
//// set default mDNS instance name
// ESP_ERROR_CHECK(mdns_instance_name_set("ESP32 Branch Lamp"));

//// structure with TXT records
//[>mdns_txt_item_t serviceTxtData[3] = {<]
//[>{"board", "esp32"}, {"u", "user"}, {"p", "password"}};<]

//// initialize service
//// add another TXT item
//[>ESP_ERROR_CHECK(mdns_service_txt_item_set("_http", "_tcp", "path", "/"));<]
//// change TXT item value
//[>ESP_ERROR_CHECK(mdns_service_txt_item_set("_http", "_tcp", "u", "admin"));<]
//}

void app_main(void) {
  static httpd_handle_t server = NULL;
  SEMA_TIME_LAST_MSG = xSemaphoreCreateBinary();
  SEMA_CURRENT_LEVELS = xSemaphoreCreateBinary();
  /*ESP_LOGI(TAG, "Webui: %s", webui);*/

  CURRENT_LEVELS = calloc(4, LEVELS_SIZE);

  /*
   * Prepare and set configuration of timers
   * that will be used by LED Controller
   */
  ledc_timer_config_t ledc_timer = {
      .duty_resolution = LEDC_TIMER_13_BIT, // resolution of PWM duty
      .freq_hz = 5000,                      // frequency of PWM signal
      .speed_mode = LEDC_HS_MODE,           // timer mode
      .timer_num = LEDC_HS_TIMER,           // timer index
      .clk_cfg = LEDC_AUTO_CLK,             // Auto select the source clock
  };

  // Set configuration of timer0 for high speed channels
  ledc_timer_config(&ledc_timer);

  ESP_ERROR_CHECK(nvs_flash_init());

  CURRENT_LEVELS = nvs_read_levels("last");
  ESP_LOGI(TAG, "Last levels %d %d %d %d", *CURRENT_LEVELS,
           *(CURRENT_LEVELS + 1), *(CURRENT_LEVELS + 2), *(CURRENT_LEVELS + 3));

  ledc_fade_func_install(0);

  uint32_t duties[4] = {0};
  for (int i = 0; i < 2; i++) {
    ESP_LOGI(TAG, "LOL %d", *(CURRENT_LEVELS + i * 2));
    ESP_LOGI(TAG, "LOL2 %d", *(CURRENT_LEVELS + i * 2));
    duties[i * 2] =
        lightLevelToDuty(2 * ((float)*(CURRENT_LEVELS + i * 2)) *
                         (65535 - ((float)*(CURRENT_LEVELS + i * 2 + 1))) /
                         ((float)65535 * 65535));
    duties[i * 2 + 1] = lightLevelToDuty(
        2 * ((float)*(CURRENT_LEVELS + i * 2)) *
        ((float)*(CURRENT_LEVELS + i * 2 + 1)) / ((float)65535 * 65535));
  }
  ESP_LOGI(TAG, "Last duties %d %d %d %d", duties[0], duties[1], duties[2],
           duties[3]);
  // Initialize PWM channels
  for (int i = 0; i < 4; i++) {
    ledc_channel_config_t ledc_channel = {.channel = LED_CHANNELS[i],
                                          .duty = duties[i],
                                          .gpio_num = LED_GPIOS[i],
                                          .speed_mode = LEDC_HS_MODE,
                                          .hpoint = 0,
                                          .timer_sel = LEDC_HS_TIMER};
    ledc_channel_config(&ledc_channel);
  }

  /*setLight(0, *CURRENT_LEVELS, *(CURRENT_LEVELS + 1));*/
  /*setLight(1, *(CURRENT_LEVELS + 2), *(CURRENT_LEVELS + 3));*/
  // free(startup_light_levels);
  ESP_ERROR_CHECK(esp_netif_init());
  ESP_ERROR_CHECK(esp_event_loop_create_default());

  /*initialise_mdns();*/

  ESP_ERROR_CHECK(example_connect());

/* Register event handlers to stop the server when Wi-Fi or Ethernet is
 * disconnected, and re-start it upon connection.
 */
#ifdef CONFIG_EXAMPLE_CONNECT_WIFI
  ESP_ERROR_CHECK(esp_event_handler_register(IP_EVENT, IP_EVENT_STA_GOT_IP,
                                             &connect_handler, &server));
  ESP_ERROR_CHECK(esp_event_handler_register(
      WIFI_EVENT, WIFI_EVENT_STA_DISCONNECTED, &disconnect_handler, &server));
#endif // CONFIG_EXAMPLE_CONNECT_WIFI
#ifdef CONFIG_EXAMPLE_CONNECT_ETHERNET
  ESP_ERROR_CHECK(esp_event_handler_register(IP_EVENT, IP_EVENT_ETH_GOT_IP,
                                             &connect_handler, &server));
  ESP_ERROR_CHECK(esp_event_handler_register(
      ETH_EVENT, ETHERNET_EVENT_DISCONNECTED, &disconnect_handler, &server));
#endif // CONFIG_EXAMPLE_CONNECT_ETHERNET

  /* [> Start the server for the first time <] */
  server = start_webserver();
  ESP_LOGI(TAG, "After start_webserver");
}

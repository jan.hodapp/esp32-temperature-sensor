#include "http_request.h"

#define MAX_HTTP_RECV_BUFFER 512
#define MAX_HTTP_OUTPUT_BUFFER 2048
#define CONFIG_EXAMPLE_HTTP_ENDPOINT "ntfy.sh"
#define CONFIG_EXAMPLE_PATH "/florian_temperatur_sensor"
// #define CONFIG_EXAMPLE_PATH "/jan_temperatur_sensor"

static const char *TAG = "http client";

extern const char
    lets_encrypt_pem_start[] asm("_binary_lets_encrypt_r3_pem_start");

esp_err_t _http_event_handler(esp_http_client_event_t *evt) {
  static char *output_buffer; // Buffer to store response of http request from
                              // event handler
  static int output_len;      // Stores number of bytes read
  int mbedtls_err = 0;
  esp_err_t err;
  switch (evt->event_id) {
  case HTTP_EVENT_ERROR:
    ESP_LOGD(TAG, "HTTP_EVENT_ERROR");
    break;
  case HTTP_EVENT_ON_CONNECTED:
    ESP_LOGD(TAG, "HTTP_EVENT_ON_CONNECTED");
    break;
  case HTTP_EVENT_HEADER_SENT:
    ESP_LOGD(TAG, "HTTP_EVENT_HEADER_SENT");
    break;
  case HTTP_EVENT_ON_HEADER:
    ESP_LOGD(TAG, "HTTP_EVENT_ON_HEADER, key=%s, value=%s", evt->header_key,
             evt->header_value);
    break;
  case HTTP_EVENT_ON_DATA:
    ESP_LOGD(TAG, "HTTP_EVENT_ON_DATA, len=%d", evt->data_len);
    /*
     *  Check for chunked encoding is added as the URL for chunked encoding used
     * in this example returns binary data. However, event handler can also be
     * used in case chunked encoding is used.
     */
    if (!esp_http_client_is_chunked_response(evt->client)) {
      // If user_data buffer is configured, copy the response into the buffer
      int copy_len = 0;
      if (evt->user_data) {
        copy_len = MIN(evt->data_len, (MAX_HTTP_OUTPUT_BUFFER - output_len));
        if (copy_len) {
          // TODO
          // memcpy(evt->user_data + output_len, evt->data, copy_len);
        }
      } else {
        const int buffer_len = esp_http_client_get_content_length(evt->client);
        if (output_buffer == NULL) {
          output_buffer = (char *)malloc(buffer_len);
          output_len = 0;
          if (output_buffer == NULL) {
            ESP_LOGE(TAG, "Failed to allocate memory for output buffer");
            return ESP_FAIL;
          }
        }
        copy_len = MIN(evt->data_len, (buffer_len - output_len));
        if (copy_len) {
          memcpy(output_buffer + output_len, evt->data, copy_len);
        }
      }
      output_len += copy_len;
    }

    break;
  case HTTP_EVENT_ON_FINISH:
    ESP_LOGD(TAG, "HTTP_EVENT_ON_FINISH");
    if (output_buffer != NULL) {
      // Response is accumulated in output_buffer. Uncomment the below line to
      // print the accumulated response ESP_LOG_BUFFER_HEX(TAG, output_buffer,
      // output_len);
      free(output_buffer);
      output_buffer = NULL;
    }
    output_len = 0;
    break;
  case HTTP_EVENT_DISCONNECTED:
    ESP_LOGI(TAG, "HTTP_EVENT_DISCONNECTED");
    err = esp_tls_get_and_clear_last_error((esp_tls_error_handle_t)evt->data,
                                           &mbedtls_err, NULL);
    if (err != 0) {
      ESP_LOGI(TAG, "Last esp error code: 0x%x", err);
      ESP_LOGI(TAG, "Last mbedtls failure: 0x%x", mbedtls_err);
    }
    if (output_buffer != NULL) {
      free(output_buffer);
      output_buffer = NULL;
    }
    output_len = 0;
    break;
  case HTTP_EVENT_REDIRECT:
    ESP_LOGD(TAG, "HTTP_EVENT_REDIRECT");
    esp_http_client_set_header(evt->client, "From", "user@example.com");
    esp_http_client_set_header(evt->client, "Accept", "text/html");
    esp_http_client_set_redirection(evt->client);
    break;
  }
  return ESP_OK;
}

static void https_with_hostname_path(std::string *post_data) {
  esp_http_client_config_t config = {
      .host = CONFIG_EXAMPLE_HTTP_ENDPOINT,
      .path = CONFIG_EXAMPLE_PATH,
      .cert_pem = lets_encrypt_pem_start,
      .event_handler = _http_event_handler,
      .transport_type = HTTP_TRANSPORT_OVER_SSL,
  };
  esp_http_client_handle_t client = esp_http_client_init(&config);
  esp_http_client_set_method(client, HTTP_METHOD_POST);
  esp_http_client_set_post_field(client, post_data->data(),
                                 post_data->length());

  // POST
  esp_err_t err = esp_http_client_perform(client);
  if (err == ESP_OK) {
    ESP_LOGI(TAG, "HTTP POST Status = %d, content_length = %" PRIu64,
             esp_http_client_get_status_code(client),
             esp_http_client_get_content_length(client));
  } else {
    ESP_LOGE(TAG, "HTTP POST request failed: %s", esp_err_to_name(err));
  }
  esp_http_client_cleanup(client);
}

// std::string get_request(std::string *host, std::string *path) {
std::string get_request() {
  char output_buffer[MAX_HTTP_OUTPUT_BUFFER] = {
      0}; // Buffer to store response of http request
  int content_length = 0;
  esp_http_client_config_t config = {
      //.host = host->c_str(),
      //.path = path->c_str(),
      .host = "whitemilan.com",
      .path = "/firmware/temperature_sensor/",
      .cert_pem = lets_encrypt_pem_start,
      .transport_type = HTTP_TRANSPORT_OVER_SSL,
  };

  ESP_LOGI(TAG, "Make config");
  esp_http_client_handle_t client = esp_http_client_init(&config);
  ESP_LOGI(TAG, "Make config");

  esp_http_client_set_method(client, HTTP_METHOD_GET);
  ESP_LOGI(TAG, "Make config");
  esp_err_t err = esp_http_client_open(client, 0);
  ESP_LOGI(TAG, "Make config");
  if (err != ESP_OK) {
    ESP_LOGE(TAG, "Failed to open HTTP connection: %s", esp_err_to_name(err));
  } else {
    content_length = esp_http_client_fetch_headers(client);
    if (content_length < 0) {
      ESP_LOGE(TAG, "HTTP client fetch headers failed");
    } else {
      int data_read = esp_http_client_read_response(client, output_buffer,
                                                    MAX_HTTP_OUTPUT_BUFFER);
      if (data_read >= 0) {
        ESP_LOGI(TAG, "HTTP GET Status = %d, content_length = %" PRIu64,
                 esp_http_client_get_status_code(client),
                 esp_http_client_get_content_length(client));
        ESP_LOG_BUFFER_HEX(TAG, output_buffer, data_read);
      } else {
        ESP_LOGE(TAG, "Failed to read response");
      }
    }
  }
  ESP_LOGI(TAG, "blub");
  auto result = std::string(output_buffer);
  esp_http_client_close(client);
  return result;
}

static void http_task(void *pvParameters) {
  std::string *post_data = static_cast<std::string *>(pvParameters);
  https_with_hostname_path(post_data);
  vTaskDelete(NULL);
}

void set_time() {
  struct timeval tv = {
      .tv_sec = 1509449941,
  };
  struct timezone tz = {0, 0};
  settimeofday(&tv, &tz);

  /* Start SNTP service */
  esp_sntp_config_t config = ESP_NETIF_SNTP_DEFAULT_CONFIG("time.windows.com");
  esp_netif_sntp_init(&config);
  if (esp_netif_sntp_sync_wait(pdMS_TO_TICKS(10000)) != ESP_OK) {
    printf("Failed to update system time, continuing");
  }
  esp_netif_deinit();
}

void post(std::string *payload) {
  xTaskCreate(&http_task, "http_test_task", 8192, payload, 5, NULL);
}

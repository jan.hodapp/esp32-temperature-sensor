#include "max6675.h"
static const char *TAG = "max6675";

#define HIGH (int)1
#define LOW (int)0

#define GPIO_OUTPUT_PIN_SEL
#define GPIO_INPUT_PIN_SEL 1ULL << GPIO_MISO

#define ESP_INTR_FLAG_DEFAULT 0

MAX6675::MAX6675(gpio_num_t gpio_sclk, gpio_num_t gpio_cs, gpio_num_t gpio_miso)
    : sclk{gpio_sclk}, miso{gpio_miso}, cs{gpio_cs} {

  // mutexValue = xSemaphoreCreateMutex();

  // if (mutexValue == NULL) {
  // ESP_LOGE(TAG, "Semaphore couldn't be created.");
  //[> The semaphore was created successfully and
  // can be used. */
  //}
  // define pin modes
  ESP_LOGI(TAG, "Constructor, %d", cs);
  gpio_config_t io_conf = {};
  // disable interrupt
  io_conf.intr_type = GPIO_INTR_DISABLE;
  // set as output mode
  io_conf.mode = GPIO_MODE_OUTPUT;
  // bit mask of the pins that you want to set,e.g.GPIO18/19
  io_conf.pin_bit_mask = ((1ULL << sclk) | (1ULL << cs));
  // disable pull-down mode
  io_conf.pull_down_en = GPIO_PULLDOWN_DISABLE;
  // disable pull-up mode
  io_conf.pull_up_en = GPIO_PULLUP_DISABLE;
  // configure GPIO with the given settings
  gpio_config(&io_conf);

  // interrupt of rising edge
  io_conf.intr_type = GPIO_INTR_POSEDGE;
  // bit mask of the pins, use GPIO4/5 here
  io_conf.pin_bit_mask = 1ULL << miso;
  // set as input mode
  io_conf.mode = GPIO_MODE_INPUT;
  // enable pull-up mode
  io_conf.pull_up_en = GPIO_PULLUP_ENABLE;
  gpio_config(&io_conf);

  gpio_set_level(cs, HIGH);
}

std::optional<float> MAX6675::readCelsius(void) {
  uint16_t v;

  gpio_set_level(cs, LOW);
  vTaskDelay(10 / portTICK_PERIOD_MS);

  v = spiread();
  v <<= 8;
  v |= spiread();

  gpio_set_level(cs, HIGH);

  if (v & 0x4) {
    ESP_LOGE(TAG, "No thermocouple attached");
    // return 100 + 10. * (float(esp_random()) / float(UINT32_MAX));
    return {};
  }

  v >>= 3;
  auto begin = std::begin(ys);
  auto end = std::end(ys);
  std::rotate(begin, std::next(begin), end);

  ys[29] = v * 0.25;

  // ESP_LOGI(TAG, "\n\nys");
  // for (float y : ys){
  // ESP_LOGI(TAG, "%f", y);
  //}
  // ESP_LOGI(TAG, "--------------------------------------\n");

  uint8_t n = 200;
  float x_bar = (1. / 2.) * (n - 1) * (n) / n;
  float y_bar = std::accumulate(begin, end, 0.f) / n;
  float x2_bar = ((n - 1) * (n) * (2. * (n - 1) + 1)) / 6. / n;

  // ESP_LOGI(TAG, "x_bar: %f, y_bar: %f, x2_bar: %f", x_bar, y_bar, x2_bar);
  float s_xx = 0, s_xy = 0, s_xx2 = 0, s_x2x2 = 0, s_x2y = 0;
  for (std::size_t x = 0; x < ys.size(); ++x) {
    float y = ys[x];
    s_xx += (x - x_bar) * (x - x_bar);
    s_xy += (x - x_bar) * (y - y_bar);
    s_xx2 += (x - x_bar) * (x * x - x2_bar);
    s_x2x2 += (x * x - x2_bar) * (x * x - x2_bar);
    s_x2y += (x * x - x2_bar) * (y - y_bar);
  }
  float b = (s_xy * s_x2x2 - s_x2y * s_xx2) / (s_xx * s_x2x2 - s_xx2 * s_xx2);
  float c = (s_x2y * s_xx - s_xy * s_xx2) / (s_xx * s_x2x2 - s_xx2 * s_xx2);
  float a = y_bar - b * x_bar - c * x2_bar;
  float result = a + b * (n - 1) + c * (n - 1) * (n - 1);
  // ESP_LOGI(TAG, "a: %f, b: %f, c: %f, result: %f", a, b, c, result);
  return result;
}

uint8_t MAX6675::spiread() {
  int8_t i;
  uint8_t d = 0;

  for (i = 7; i >= 0; i--) {
    gpio_set_level(sclk, LOW);
    vTaskDelay(10 / portTICK_PERIOD_MS);

    if (gpio_get_level(miso) == 1) {
      // set the bit to 0 no matter what
      d |= (1 << i);
    }

    gpio_set_level(sclk, HIGH);
    vTaskDelay(10 / portTICK_PERIOD_MS);
  }

  return d;
}

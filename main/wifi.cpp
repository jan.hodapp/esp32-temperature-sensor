#include "wifi.h"
#include "esp_netif_ip_addr.h"
#include "http_request.h"
#include <algorithm>

#define ESP_WIFI_SCAN_AUTH_MODE_THRESHOLD WIFI_AUTH_WPA2_PSK
#define ESP_WIFI_SAE_MODE WPA3_SAE_PWE_BOTH
#define EXAMPLE_H2E_IDENTIFIER "/0"

#define AP_SSID "Temperature Sensor 9000"
#define AP_WIFI_CHANNEL 11
#define AP_STA_CONN 10

static EventGroupHandle_t s_wifi_event_group;

#define WIFI_CONNECTED_BIT BIT0
#define WIFI_FAIL_BIT BIT1

#define EXAMPLE_ESP_MAXIMUM_RETRY 2
#define DEFAULT_SCAN_LIST_SIZE 20
static int s_retry_num = 0;

static const char *TAG = "wifi";

Wifi::Wifi(WifiConfiguration configuration, esp_event_loop_handle_t app)
    : state(State::Unconnected), app(app), configuration(configuration) {

  ESP_ERROR_CHECK(esp_event_handler_instance_register(
      WIFI_EVENT, WIFI_EVENT_STA_START,
      [](void *arg, esp_event_base_t event_base, int32_t event_id,
         void *event_data) {
        ESP_LOGI(TAG, "WIFI_EVENT_STA_START");
        esp_wifi_connect();
      },
      this, NULL));
  ESP_ERROR_CHECK(esp_event_handler_instance_register(
      WIFI_EVENT, WIFI_EVENT_STA_DISCONNECTED,
      [](void *arg, esp_event_base_t event_base, int32_t event_id,
         void *event_data) {
        ESP_LOGI(TAG, "Event: WIFI_EVENT_STA_DISCONNECTED");
        esp_wifi_connect();
        ESP_LOGI(TAG, "retry to connect to the AP");
        xEventGroupSetBits(s_wifi_event_group, WIFI_FAIL_BIT);
      },
      this, NULL));
  ESP_ERROR_CHECK(esp_event_handler_instance_register(
      IP_EVENT, IP_EVENT_STA_GOT_IP,
      [](void *arg, esp_event_base_t event_base, int32_t event_id,
         void *event_data) {
        ip_event_got_ip_t *event = (ip_event_got_ip_t *)event_data;
        ESP_LOGI(TAG, "got ip:" IPSTR, IP2STR(&event->ip_info.ip));
        set_time();
        s_retry_num = 0;
        xEventGroupSetBits(s_wifi_event_group, WIFI_CONNECTED_BIT);
        static_cast<Wifi *>(event_data)->state = State::IpFromStation;
        write_app_data<WifiConfiguration>(
            "wifi_config", &static_cast<Wifi *>(arg)->configuration);
        static std::string message;
        message.resize(100); 
        sprintf(message.data(),
                "Temperatur Sensor ist mit dem WLAN verbunden, erreichbar unter: %d.%d.%d.%d", IP2STR(&event->ip_info.ip));
        post(&message);
      },
      this, NULL));

  ESP_ERROR_CHECK(esp_netif_init());
  esp_netif_config_t cfg = ESP_NETIF_DEFAULT_WIFI_AP();
  ap = esp_netif_new(&cfg);
  assert(ap);
  cfg = ESP_NETIF_DEFAULT_WIFI_STA();
  sta = esp_netif_new(&cfg);
  assert(sta);

  esp_wifi_set_storage(WIFI_STORAGE_RAM);
}

esp_err_t Wifi::init_connection_to_station(WifiConfiguration c) {
  if (sntp_enabled()) {
    sntp_stop();
  }
  if (state == State::IpFromStation) {
    ESP_ERROR_CHECK(esp_wifi_disconnect());
    state = State::ConnectedToStation;
  }
  if (state == State::AccessPoint) {
    ESP_ERROR_CHECK(esp_wifi_stop());
    ESP_ERROR_CHECK(esp_wifi_set_mode(WIFI_MODE_NULL));
  }
  configuration = c;
  s_wifi_event_group = xEventGroupCreate();

  if (sta == NULL) {
  }

  ESP_ERROR_CHECK(esp_netif_attach_wifi_station(sta));
  ESP_ERROR_CHECK(esp_wifi_set_default_wifi_sta_handlers());

  wifi_init_config_t cfg = WIFI_INIT_CONFIG_DEFAULT();
  ESP_ERROR_CHECK(esp_wifi_init(&cfg));

  wifi_config_t wifi_config = {
      .sta =
          {
              .threshold =
                  {
                      .authmode = ESP_WIFI_SCAN_AUTH_MODE_THRESHOLD,
                  },
              .sae_pwe_h2e = ESP_WIFI_SAE_MODE,
              .sae_h2e_identifier = EXAMPLE_H2E_IDENTIFIER,
          },
  };
  memcpy(wifi_config.sta.ssid, configuration.ssid, 32);
  memcpy(wifi_config.sta.password, configuration.password, 64);

  ESP_ERROR_CHECK(esp_wifi_set_mode(WIFI_MODE_STA));
  ESP_ERROR_CHECK(esp_wifi_set_config(WIFI_IF_STA, &wifi_config));
  ESP_ERROR_CHECK(esp_wifi_start());

  ESP_LOGI(TAG, "Wifi config: %s %s", wifi_config.sta.ssid,
           wifi_config.sta.password);
  if (esp_wifi_set_mode(WIFI_MODE_STA) != ESP_OK)
    return ESP_FAIL;
  if (esp_wifi_set_config(WIFI_IF_STA, &wifi_config) != ESP_OK)
    return ESP_FAIL;
  if (esp_wifi_start() != ESP_OK)
    return ESP_FAIL;

  ESP_LOGI(TAG, "wifi_init_sta finished.");

  // Waiting until either the connection is established (WIFI_CONNECTED_BIT) or
  // connection failed for the maximum number of re-tries (WIFI_FAIL_BIT). The
  // bits are set by event_handler() (see above)
  EventBits_t bits = xEventGroupWaitBits(s_wifi_event_group,
                                         WIFI_CONNECTED_BIT | WIFI_FAIL_BIT,
                                         pdFALSE, pdFALSE, portMAX_DELAY);

  state = State::ConnectedToStation;
  // xEventGroupWaitBits() returns the bits before the call returned, hence we
  // can test which event actually happened.
  if (bits & WIFI_CONNECTED_BIT) {
    ESP_LOGI(TAG, "connected to ap SSID:%s", configuration.ssid);
    return ESP_OK;
  } else if (bits & WIFI_FAIL_BIT) {
    ESP_LOGI(TAG, "Failed to connect to SSID:%s", configuration.ssid);
    return ESP_FAIL;
  } else {
    ESP_LOGE(TAG, "UNEXPECTED EVENT");
    return ESP_FAIL;
  }
}

esp_err_t Wifi::init_access_point() {
  if (sntp_enabled()) {
    sntp_stop();
  }
  if (state == State::AccessPoint)
    return ESP_OK;
  if (state == State::IpFromStation) {
    ESP_ERROR_CHECK(esp_wifi_disconnect());
    state = State::ConnectedToStation;
  }
  if (state == State::ConnectedToStation) {
    ESP_ERROR_CHECK(esp_wifi_stop());
    ESP_ERROR_CHECK(esp_wifi_set_mode(WIFI_MODE_NULL));
  }
  ESP_ERROR_CHECK(esp_netif_attach_wifi_ap(ap));
  ESP_ERROR_CHECK(esp_wifi_set_default_wifi_ap_handlers());

  wifi_init_config_t cfg = WIFI_INIT_CONFIG_DEFAULT();
  ESP_ERROR_CHECK(esp_wifi_init(&cfg));

  wifi_config_t wifi_config = {
      .ap =
          {
              .ssid = AP_SSID,
              .password = "",
              .ssid_len = static_cast<uint8_t>(strlen(AP_SSID)),
              .channel = AP_WIFI_CHANNEL,
              .authmode = WIFI_AUTH_WPA3_PSK,
              .max_connection = AP_STA_CONN,
              .pmf_cfg =
                  {
                      .required = true,
                  },
              .sae_pwe_h2e = WPA3_SAE_PWE_BOTH,
          },
  };
  wifi_config.ap.authmode = WIFI_AUTH_OPEN;

  if (esp_wifi_set_mode(WIFI_MODE_AP) != ESP_OK)
    return ESP_FAIL;
  if (esp_wifi_set_config(WIFI_IF_AP, &wifi_config) != ESP_OK)
    return ESP_FAIL;
  if (esp_wifi_start() != ESP_OK)
    return ESP_FAIL;

  ESP_LOGI(TAG, "wifi_init_softap finished. SSID:%s channel:%d", AP_SSID,
           AP_WIFI_CHANNEL);
  state = State::AccessPoint;
  return ESP_OK;
}

std::set<std::string> *Wifi::scan() {
  if (sntp_enabled()) {
    sntp_stop();
  }
  if (state == State::AccessPoint || state == State::ConnectedToStation) {
    ESP_LOGI(TAG, "Access Point");
    ESP_ERROR_CHECK(esp_wifi_stop());
  }
  ESP_ERROR_CHECK(esp_netif_attach_wifi_station(sta));
  ESP_ERROR_CHECK(esp_wifi_set_default_wifi_sta_handlers());

  wifi_init_config_t cfg = WIFI_INIT_CONFIG_DEFAULT();
  ESP_ERROR_CHECK(esp_wifi_init(&cfg));

  uint16_t number = DEFAULT_SCAN_LIST_SIZE;
  wifi_ap_record_t ap_info[DEFAULT_SCAN_LIST_SIZE];
  uint16_t ap_count = 0;
  memset(ap_info, 0, sizeof(ap_info));

  ESP_ERROR_CHECK(esp_wifi_set_mode(WIFI_MODE_STA));
  ESP_ERROR_CHECK(esp_wifi_start());

  ESP_ERROR_CHECK(esp_wifi_disconnect());
  esp_wifi_scan_start(NULL, true);

  ESP_ERROR_CHECK(esp_wifi_scan_get_ap_records(&number, ap_info));
  ESP_ERROR_CHECK(esp_wifi_scan_get_ap_num(&ap_count));
  ESP_LOGI(TAG, "Total APs scanned = %u", ap_count);

  scanned_ssids.clear();
  for (auto ap_i : ap_info) {
    scanned_ssids.insert(
        static_cast<std::string>(reinterpret_cast<char *>(ap_i.ssid)));
  };

  for (int i = 0; (i < DEFAULT_SCAN_LIST_SIZE) && (i < ap_count); i++) {
    ESP_LOGI(TAG, "SSID \t\t%s", ap_info[i].ssid);
    ESP_LOGI(TAG, "RSSI \t\t%d", ap_info[i].rssi);
    ESP_LOGI(TAG, "Channel \t\t%d\n", ap_info[i].primary);
  }
  return &scanned_ssids;
}

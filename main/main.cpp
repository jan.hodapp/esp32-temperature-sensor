#include "configuration.h"
#include "esp_log.h"
#include "esp_task.h"
#include "event.h"
#include "http_request.h"
#include "iot_button.h"
#include "max6675.h"
#include "nvs.h"
#include "nvs_flash.h"
#include "ota.h"
#include "semantic_version.h"
#include "server.h"
#include "wifi.h"
#include <atomic>
#include <memory>
#include <string>

#define TIME_PERIOD (86400000000ULL)
#define GPIO_SCLK GPIO_NUM_13
#define GPIO_CS GPIO_NUM_14
#define GPIO_MISO GPIO_NUM_15

static const char *tag = "main";

enum AppState { Running, ConfiguringWifi, Starting };

extern "C" void app_main(void) {

  ESP_LOGI(tag, "Version: %s", FIRMWARE_VERSION.to_string().c_str());
  esp_err_t ret = nvs_flash_init();
  if (ret == ESP_ERR_NVS_NO_FREE_PAGES ||
      ret == ESP_ERR_NVS_NEW_VERSION_FOUND) {
    ESP_ERROR_CHECK(nvs_flash_erase());
    ret = nvs_flash_init();
  }
  ESP_ERROR_CHECK(ret);

  ESP_ERROR_CHECK(esp_event_loop_create_default());

  ESP_LOGI(tag, "ESP_WIFI_MODE_STA");
  esp_log_level_set("*", ESP_LOG_DEBUG);

  auto wifi_configuration =
      std::atomic(load_app_data<WifiConfiguration>("wifi_config"));
  ESP_LOGI(tag, "Configuration: %s %s", wifi_configuration.load().ssid,
           wifi_configuration.load().password);
  auto app = std::atomic(initialize_app_event_loop());

  MAX6675 thermocouple(GPIO_SCLK, GPIO_CS, GPIO_MISO);
  auto temperature = std::atomic<float>(0);

  Wifi wifi(wifi_configuration, app);
  std::atomic<float> alarm_temperature = load_app_data<float>("alarm_temp");
  httpd_handle_t server = NULL;

  auto state = std::atomic(AppState::Starting);
  auto wifi_handles = std::make_tuple(&wifi_configuration, &state);
  ESP_ERROR_CHECK(esp_event_handler_instance_register_with(
      app, APP, App_Event::Wifi_Configuration,
      [](void *arg, esp_event_base_t event_base, int32_t event_id,
         void *event_data) {
        auto handles = static_cast<
            std::tuple<WifiConfiguration *, std::atomic<AppState> *> *>(arg);
        auto wifi_configuration = std::get<WifiConfiguration *>(*handles);
        auto state = std::get<std::atomic<AppState> *>(*handles);
        *wifi_configuration = *static_cast<WifiConfiguration *>(event_data);
        *state = AppState::Running;
      },
      &wifi_handles, NULL));

  ESP_ERROR_CHECK(esp_event_handler_instance_register_with(
      app, APP, App_Event::Temperature_Configuration,
      [](void *arg, esp_event_base_t event_base, int32_t event_id,
         void *event_data) {
        ESP_LOGI(tag, "Got alarm temperature %f",
                 *static_cast<float *>(event_data));
        auto alarm_temperature = static_cast<std::atomic<float> *>(arg);
        alarm_temperature->store(*static_cast<float *>(event_data));
        write_app_data("alarm_temp", alarm_temperature);
      },
      &alarm_temperature, NULL));

  // create gpio button
  button_config_t gpio_btn_cfg = {
      .type = BUTTON_TYPE_GPIO,
      .long_press_time = CONFIG_BUTTON_LONG_PRESS_TIME_MS,
      .short_press_time = CONFIG_BUTTON_SHORT_PRESS_TIME_MS,
      .gpio_button_config =
          {
              .gpio_num = 0,
              .active_level = 0,
          },
  };

  button_handle_t gpio_btn = iot_button_create(&gpio_btn_cfg);
  if (NULL == gpio_btn) {
    ESP_LOGE(tag, "Button create failed");
  }
  iot_button_register_cb(
      gpio_btn, BUTTON_SINGLE_CLICK,
      [](void *arg, void *usr_data) {
        ESP_LOGI(tag, "Button pressed");
        auto state = static_cast<AppState *>(usr_data);
        *state = AppState::ConfiguringWifi;
      },
      &state);
  iot_button_register_cb(
      gpio_btn, BUTTON_LONG_PRESS_UP,
      [](void *arg, void *usr_data) {
        ESP_LOGI(tag, "Button long pressed");
        WifiConfiguration configuration("", "");
        write_app_data<WifiConfiguration>("wifi_config", &configuration);
        esp_restart();
      },
      NULL);

  if (wifi_configuration.load().ssid[0] == '\0') {
    state = AppState::ConfiguringWifi;
  } else {
    state = AppState::Running;
  }
  AppState last = AppState::Starting;
  std::optional<float> op_temperature;

  bool above_alarm = false;
  while (true) {
    switch (state) {
    case AppState::Running:
      // Entering
      if (last != state) {
        last = state;
        ESP_LOGI(tag, "Entering Running");
        if (server) {
          ESP_LOGI(tag, "Server exists");
          webserver_stop(server);
        }
        wifi.init_connection_to_station(wifi_configuration);
        std::set<std::string> ssids;
        server =
            webserver_start(&app, &ssids, &temperature, &alarm_temperature);
        ota();
        above_alarm = false;
      }

      // Running
      ESP_LOGI(tag, "State: Running");
      ESP_LOGI(tag, "app: %p", &app);

      op_temperature = thermocouple.readCelsius();
      if (op_temperature.has_value()) {
        temperature = op_temperature.value();
      } else {
        break;
      }

      ESP_LOGI(tag, "Temperature: %6.1f, Alarm: %6.1f", temperature.load(),
               alarm_temperature.load());
      if (temperature >= alarm_temperature) {
        above_alarm = true;
      } else {
        if (above_alarm) {
          above_alarm = false;
          ESP_LOGI(tag, "Alarm");
          static std::string data;
          data.resize(64);
          sprintf(data.data(), "Alarm: Temperatur ist bei %6.1lf°C",
                  temperature.load());
          post(&data);
        }
      }
      break;
    case AppState::ConfiguringWifi:
      // Entering
      if (last != state) {
        last = state;
        ESP_LOGI(tag, "Entering Configuring");

        if (server) {
          ESP_LOGI(tag, "Server exists");
          webserver_stop(server);
        }
        auto ssids = wifi.scan();
        wifi.init_access_point();
        server = webserver_start(&app, ssids, &temperature, &alarm_temperature);
      }
      ESP_LOGI(tag, "State: Configuring");
      vTaskDelay(1000 / portTICK_PERIOD_MS);
      break;
    default:
      ESP_LOGI(tag, "Default");
      state = AppState::Running;
      break;
    }
    vTaskDelay(10000 / portTICK_PERIOD_MS);
  }
}

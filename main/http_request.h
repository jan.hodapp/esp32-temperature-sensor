#pragma once

#include "esp_err.h"
#include "esp_http_client.h"
#include "esp_log.h"
#include "esp_netif.h"
#include "esp_netif_sntp.h"
#include "esp_tls.h"
#include <string>
#include <sys/param.h>

void post(std::string *payload);
std::string get_request();
void set_time();

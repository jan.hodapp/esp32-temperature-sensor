default:
  just --list

build: 
  idf.py build

flash:
  idf.py -p /dev/ttyUSB0 build flash

run:
  idf.py -p /dev/ttyUSB0 build flash monitor

monitor:
  idf.py -p /dev/ttyUSB0 monitor

clean:
  idf.py fullclean

configure:
  idf.py menuconfig
